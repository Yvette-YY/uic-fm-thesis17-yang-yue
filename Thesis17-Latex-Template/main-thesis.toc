\contentsline {chapter}{Title Page}{\nbi i}
\contentsline {chapter}{Authorization Page}{\nbi ii}
\contentsline {chapter}{Signature Page}{\nbi iii}
\contentsline {chapter}{Dedication}{\nbi iv}
\contentsline {chapter}{Acknowledgments}{\nbi v}
\contentsline {chapter}{Table of Contents}{\nbi vi}
\contentsline {chapter}{List of Figures}{\nbi viii}
\contentsline {chapter}{List of Tables}{\nbi ix}
\contentsline {chapter}{Abstract}{\nbi x}
\contentsline {chapter}{{Chapter 1}\hspace {1em}Introduction}{1}
\contentsline {chapter}{{Chapter 2}\hspace {1em}Related Work}{5}
\contentsline {section}{\numberline {2.1}Categorization for Techniques}{5}
\contentsline {section}{\numberline {2.2}Categorization for Similar Problems}{5}
\contentsline {chapter}{{Chapter 3}\hspace {1em}Solution}{6}
\contentsline {section}{\numberline {3.1}Section 1}{6}
\contentsline {subsection}{\numberline {3.1.1}Subsection 1}{7}
\contentsline {section}{\numberline {3.2}Section 2}{7}
\contentsline {chapter}{{Chapter 4}\hspace {1em}Experiment Results}{8}
\contentsline {section}{\numberline {4.1}Dataset and Parameter Setting}{8}
\contentsline {section}{\numberline {4.2}Evaluation 1}{9}
\contentsline {chapter}{{Chapter 5}\hspace {1em}Summary of Research and Future Work}{10}
\contentsline {section}{\numberline {5.1}Summary of Research and Contributions}{10}
\contentsline {section}{\numberline {5.2}Future Research}{10}
\contentsline {chapter}{References}{11}
